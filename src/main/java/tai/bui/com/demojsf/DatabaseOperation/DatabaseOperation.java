package tai.bui.com.demojsf.DatabaseOperation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

import javax.faces.context.FacesContext;

import tai.bui.com.demojsf.bean.StudentBean;

public class DatabaseOperation {

	public static Statement stmtObj;
	public static Connection connObj;
	public static ResultSet resultSetObj;
	public static PreparedStatement pstmt;

	/* Method To Establish Database Connection */
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String db_url = "jdbc:mysql://localhost:3306/students", db_userName = "root", db_password = "admin";
			connObj = DriverManager.getConnection(db_url, db_userName, db_password);
			System.out.println("connect successfully");
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		}
		return connObj;
	}

	/* Method To Fetch The Student Records From Database */
	public static ArrayList<StudentBean> getStudentsListFromDB() {
		ArrayList<StudentBean> studentsList = new ArrayList<StudentBean>();
		try {
			stmtObj = getConnection().createStatement();
			resultSetObj = stmtObj.executeQuery("SELECT * FROM STUDENTS");
			while (resultSetObj.next()) {
				StudentBean stuObj = new StudentBean();
				stuObj.setId(resultSetObj.getInt("ID"));
				stuObj.setName(resultSetObj.getString("NAME"));
				stuObj.setEmail(resultSetObj.getString("EMAIL"));
				stuObj.setPassword(resultSetObj.getString("PASSWORD"));
				stuObj.setGender(resultSetObj.getString("GENDER"));
				stuObj.setAddress(resultSetObj.getString("ADDRESS"));
				studentsList.add(stuObj);
			}
			System.out.println("Total Records Fetched: " + studentsList.size());
			connObj.close();
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		}
		return studentsList;
	}

	/* Method Used To Save New Student Record In Database */
	public static String saveStudentDetailsInDB(StudentBean newStudentObj) {
		int saveResult = 0;
		String navigationResult = "";
		try {
			pstmt = getConnection().prepareStatement(
					"INSERT INTO STUDENTS (NAME, EMAIL, PASSWORD, GENDER, ADDRESS) VALUES (?, ?, ?, ?, ?)");
			pstmt.setString(1, newStudentObj.getName());
			pstmt.setString(2, newStudentObj.getEmail());
			pstmt.setString(3, newStudentObj.getPassword());
			pstmt.setString(4, newStudentObj.getGender());
			pstmt.setString(5, newStudentObj.getAddress());
			saveResult = pstmt.executeUpdate();
			connObj.close();
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		}
		if (saveResult != 0) {
			navigationResult = "studentsList.xhtml?faces-redirect=true";
		} else {
			navigationResult = "createStudent.xhtml?faces-redirect=true";
		}
		return navigationResult;
	}

	/* Method Used To Edit Student Record In Database */
	public static String editStudentRecordInDB(int studentId) {
		StudentBean editRecord = null;
		System.out.println("editStudentRecordInDB() : Student Id: " + studentId);

		/* Setting The Particular Student Details In Session */
		Map<String, Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();

		try {
			stmtObj = getConnection().createStatement();
			resultSetObj = stmtObj.executeQuery("SELECT * FROM STUDENTS WHERE ID = " + studentId);
			if (resultSetObj != null) {
				resultSetObj.next();
				editRecord = new StudentBean();
				editRecord.setId(resultSetObj.getInt("ID"));
				editRecord.setName(resultSetObj.getString("NAME"));
				editRecord.setEmail(resultSetObj.getString("EMAIL"));
				editRecord.setGender(resultSetObj.getString("GENDER"));
				editRecord.setAddress(resultSetObj.getString("ADDRESS"));
				editRecord.setPassword(resultSetObj.getString("PASSWORD"));
			}
			sessionMapObj.put("editRecordObj", editRecord);
			connObj.close();
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		}
		return "/editStudent.xhtml?faces-redirect=true";
	}

	/* Method Used To Update Student Record In Database */
	public static String updateStudentDetailsInDB(StudentBean updateStudentObj) {
		try {
			pstmt = getConnection().prepareStatement(
					"UPDATE STUDENTS SET NAME = ?, EMAIL = ?, PASSWORD = ?, GENDER = ?, ADDRESS = ? WHERE ID = ?");
			pstmt.setString(1, updateStudentObj.getName());
			pstmt.setString(2, updateStudentObj.getEmail());
			pstmt.setString(3, updateStudentObj.getPassword());
			pstmt.setString(4, updateStudentObj.getGender());
			pstmt.setString(5, updateStudentObj.getAddress());
			pstmt.setInt(6, updateStudentObj.getId());
			pstmt.executeUpdate();
			connObj.close();
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		}
		return "/studentsList.xhtml?faces-redirect=true";
	}

	/* Method Used To Delete Student Record From Database */
	public static String deleteStudentRecordInDB(int studentId) {
		System.out.println("deleteStudentRecordInDB() : Student Id: " + studentId);
		try {
			pstmt = getConnection().prepareStatement("DELETE FROM STUDENTS WHERE ID = " + studentId);
			pstmt.executeUpdate();
			connObj.close();
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		}
		return "/studentsList.xhtml?faces-redirect=true";
	}
}